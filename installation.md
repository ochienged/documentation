# Installation via Manifests

This document describes how to deploy the GitLab operator via manifests in your Kubernetes or Openshift cluster.

These steps normally are handled by OLM, the Operator Lifecycle Manager, once an operator is bundle published. However, to test the most recent operator images, users may need to install the operator using the deployment manifests available in the operator repository.

## Requirements

1. Ensure the operators it depends on are present. The GitLab operator uses the following operators:

* the `Nginx Ingress Operator` by Nginx Inc. to deploy and Ingress Controller. This should be deployed from operatorhub.io if using Kubernetes or the embedded Operator Hub on Openshift environments

* the `Cert Manager operator` to create certificates used to secure the GitLab and Registry urls

2. Clone the GitLab operator repository to your local system

```
$ git clone https://gitlab.com/gitlab-org/gl-openshift/gitlab-operator.git
$ cd gitlab-operator
```

3. Deploy the CRDs(Custom Resource Definitions) for the resources managed by the operator

```
$ ls config/deploy/apps.gitlab.com_* | xargs -n 1 kubectl apply -f
```

4. Deploy the service accounts, roles and role bindings used by the operator

```
$ ls config/deploy/gitlab-*_serviceaccount.yaml | xargs -n 1 kubectl apply -f
$ ls config/deploy/gitlab-*_clusterrole.yaml | xargs -n 1 kubectl apply -f
$ ls config/deploy/gitlab-*_clusterrolebinding.yaml | xargs -n 1 kubectl apply -f
```

5. Finally, deploy the operator

`$ kubectl apply -f gitlab-operator-manager.yaml`

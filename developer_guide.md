# Developer Guide

This developer guide aims to walk a new developer on how to setup up their environment to be able to contribute to this project.


## Setting up development environment

To setup your system for development of the operator, follow the steps below:

1. Install [golang](https://golang.org/dl/) in your environment.

`$ go version`

2. Download `operator-sdk`. Current `operator-sdk` [releases](https://github.com/operator-framework/operator-sdk/releases) can be found in the projects repository.

To check your version of operator SDK run,

`$ operator-sdk version`

3. Fork and clone the `gitlab-operator` repository into your GOPATH.

4. To contribute code to the current GitLab operator release, you will need at least operator SDK v1.0.0 .

## Deploying the Operator

The GitLab operator takes advantage of other operators to deploy run normally. Namely, prometheus, the nginx ingress controller by Nginx Inc and Cert-Manager operators.

When the operator installation is handled via OLM, the Operator Lifecycle Manager, these dependencies would be installed automatically. However, when testing changes, this will unlikely be the case, so you may need to install the operator using the manifests in the deploy directory.

## Project structure

The GitLab operator is built using the Operator SDK v1.0.0 and consequently uses the Kubebuilder v2 layout format. This is necessary to know since there was a change in project directory and some of the tooling used by operator SDK.  

  
```
λ  gitlab-operator git:(demo*) tree -dL 2 .
.
├── api
│   └── v1beta1
├── bin
├── bundle
│   ├── manifests
│   ├── metadata
│   └── tests
├── config
│   ├── certmanager
│   ├── crd
│   ├── default
│   ├── deploy
│   ├── local
│   ├── manager
│   ├── manifests
│   ├── prometheus
│   ├── rbac
│   ├── samples
│   ├── scorecard
│   └── webhook
├── controllers
│   ├── backup
│   ├── gitlab
│   ├── helm
│   ├── runner
│   └── utils
├── docs
└── hack
    └── assets
```

* The `controllers` directory contains the controller implementations for the GitLab, GitLab Runner and GitLab Backup controller.

* The `api` directory would contain the API resource definitions for the GitLab, Runner and GLBackup owned by the operator. The API definitions would be group by their API version.

The `*_types.go` file inside `api/<api_version>` would contain spec definitions and markers used to generate the Custom Resource Definitions and Cluster Service Version file used by OLM.

* The `config/samples` contains examples of the GitLab, GLBackup and Runner definitions.

* The `config/rbac` contains the roles, role bindings and service accounts needed for the operator to run. The roles should be updated through RBAC(Role Based Access Control) [markers](https://book.kubebuilder.io/reference/markers/rbac.html) inside your controllers.

An example is shown below:

`// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete`

The contents of `config/rbac/custom` were created manually and is not affected by the RBAC markers.

Most of the other contents of the config directory are automatically generated but could be modified using `kustomize`

* The `hack/assets` path contains resources that would need to be pushed inside the operator image when the container image is being built. This is where release files would go into.

### Additional Resources

#### 1. Makefile
The `Makefile` allows us to customize manage different tasks such as:
 - creating an Operator Lifecycle Manager bundle  

  `$ make bundle`
 - building a container image for the operator  

   `$ make docker-build IMG=quay.io/<username>/gitlab-operator:latest`
 - pushing the image to a container registry  

   `$ make docker-push IMG=quay.io/<username>/gitlab-operator:latest`
 - run the operator locally to test changes  

   `$ make run`
